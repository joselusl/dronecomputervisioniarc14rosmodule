//////////////////////////////////////////////////////
//  DroneTrajectoryPlannerROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneCVIARC14ROSModule.h"



using namespace std;




DroneKeypointsGridDetectorROSModule::DroneKeypointsGridDetectorROSModule() : DroneModule(droneModule::active)
{

    return;
}


DroneKeypointsGridDetectorROSModule::~DroneKeypointsGridDetectorROSModule()
{
	close();
	return;
}


int DroneKeypointsGridDetectorROSModule::setCalibCamera(std::string camCalibParamFile)
{
    this->camCalibParamFile=camCalibParamFile;
    return 1;
}

int DroneKeypointsGridDetectorROSModule::setGridParameters(std::string gridIntersectionsParamFile)
{
    this->gridIntersectionsParamFile=gridIntersectionsParamFile;
    return 1;
}

int DroneKeypointsGridDetectorROSModule::setTopicConfigs(std::string camSubsTopicName, std::string keypointsPublTopicName)
{
    this->camSubsTopicName=camSubsTopicName;
    this->keypointsPublTopicName=keypointsPublTopicName;
    return 1;
}


bool DroneKeypointsGridDetectorROSModule::init()
{
    DroneModule::init();


    /////Init cv module

    // Camera parameters
    IA14G.initializeCameraParameters(camCalibParamFile);


    // Load parameters from XML file
    // BOT-RELATED
    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(gridIntersectionsParamFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        std::cout<<"Error opening grid intersections parameter file"<<std::endl;
        return false;
    }
    /////////////////////////////////////
    // GRID-RELATED
    cv::Size gridWorkSize, DTCells;
    cv::Vec3i BGR_valuesForBG=cv::Vec3i(0, 0, 0);
    double maskWhiteRatio;
    int ransac_max_iterations, ransac_max_inlier_distance, ransac_min_inliers, clusteringRadius, y_channel_threshold;

    std::string readingValue;
    pugi::xml_node GRID = doc.child("grid");


    readingValue=GRID.child_value("width");
    std::istringstream convertedValue1(readingValue);
    convertedValue1>> gridWorkSize.width;
    //cout<<"width="<<gridWorkSize.width<<endl;

    readingValue=GRID.child_value("height");
    std::istringstream convertedValue2(readingValue);
    convertedValue2>> gridWorkSize.height;

    readingValue=GRID.child_value("horizontalCells");
    std::istringstream convertedValue3(readingValue);
    convertedValue3>> DTCells.width;
    readingValue=GRID.child_value("verticalCells");
    std::istringstream convertedValue4(readingValue);
    convertedValue4>> DTCells.height;

    readingValue=GRID.child_value("maskWhiteRatio");
    std::istringstream convertedValue5(readingValue);
    convertedValue5>> maskWhiteRatio;
    readingValue=GRID.child_value("redColorBG");
    std::istringstream convertedValue6(readingValue);
    convertedValue6>> BGR_valuesForBG[2];
    readingValue=GRID.child_value("greenColorBG");
    std::istringstream convertedValue7(readingValue);
    convertedValue7>> BGR_valuesForBG[1];
    readingValue=GRID.child_value("blueColorBG");
    std::istringstream convertedValue8(readingValue);
    convertedValue8>> BGR_valuesForBG[0];

    readingValue=GRID.child_value("ransac_max_iterations");
    std::istringstream convertedValue9(readingValue);
    convertedValue9>> ransac_max_iterations;
    readingValue=GRID.child_value("ransac_max_inlier_distance");
    std::istringstream convertedValue10(readingValue);
    convertedValue10>> ransac_max_inlier_distance;
    readingValue=GRID.child_value("ransac_min_inliers");
    std::istringstream convertedValue11(readingValue);
    convertedValue11>> ransac_min_inliers;

    readingValue=GRID.child_value("clusteringRadius");
    std::istringstream convertedValue12(readingValue);
    convertedValue12>> clusteringRadius;
	
	readingValue=GRID.child_value("Y_channel_thresh");
    std::istringstream convertedValue13(readingValue);
    convertedValue13>> y_channel_threshold;


    IA14G.initializeGridModule(gridWorkSize, BGR_valuesForBG, maskWhiteRatio, DTCells, ransac_max_iterations,
        ransac_max_inlier_distance, ransac_min_inliers, clusteringRadius, y_channel_threshold);



    //end
    return true;
}


void DroneKeypointsGridDetectorROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    DroneModule::open(nIn);


    //Camera parameters
    std::string camera_calib_param;
    ros::param::get("~camera_calib_param", camera_calib_param);
    cout<<"camera_calib_param="<<camera_calib_param<<endl;
    if ( camera_calib_param.length() == 0)
    {
        cout<<"[ROSNODE] Error with camera calibration paramaters"<<endl;
    }

    //grid parameters
    std::string grid_param;
    ros::param::get("~grid_param", grid_param);
    cout<<"grid_param="<<grid_param<<endl;
    if ( grid_param.length() == 0)
    {
        cout<<"[ROSNODE] Error with grid paramaters"<<endl;
    }

    //Camera subscription topic
    std::string camera_topic_name;
    ros::param::get("~camera_topic_name",camera_topic_name);
    cout<<"camera_topic_name="<<camera_topic_name<<endl;
    if (camera_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with video subscription topic";
    }

    //Keypoints publ topic
    std::string keypointsPublTopicName;
    ros::param::get("~keypoints_topic_name",keypointsPublTopicName);
    cout<<"keypointsPublTopicName="<<keypointsPublTopicName<<endl;
    if (keypointsPublTopicName.length()==0)
    {
        cout<<"[ROSNODE] Error with publisher topic";
    }




    if(!setCalibCamera(camera_calib_param))
        return;

    if(!setTopicConfigs(camera_topic_name,keypointsPublTopicName))
        return;

    if(!setGridParameters(grid_param))
        return;


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }



    //// TOPICS
    //Subscribers
    droneImageSubs = n.subscribe(camSubsTopicName, 1, &DroneKeypointsGridDetectorROSModule::droneImageCallback, this);



    //Publishers
    droneKeypointsPubl=n.advertise<droneMsgsROS::vectorPoints2DInt>(keypointsPublTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;

	
	//End
	return;
}


void DroneKeypointsGridDetectorROSModule::close()
{
    DroneModule::close();



    //Do stuff

    return;
}


bool DroneKeypointsGridDetectorROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool DroneKeypointsGridDetectorROSModule::startVal()
{
    //Do stuff

    //End
    return DroneModule::startVal();
}


bool DroneKeypointsGridDetectorROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool DroneKeypointsGridDetectorROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;


    //Process image
    gridIntersections.clear(); //Clean
    cv::Mat undistortedFrame;
    //cout<<"lo veo"<<endl;
    int errorCodeG=IA14G.run(droneImage, undistortedFrame, gridIntersections);
    //cout<<"no lo veo"<<endl;

#ifdef CV_DEBUG
    // show the results
    cv::Mat results=undistortedFrame.clone();
    IA14G.showResults(results, gridIntersections);
    cv::imshow("grid intersections", results);
    cv::waitKey(1);
#endif

    // check for completion of the IA14G.run()
    if(errorCodeG<0)
    {
        //return false;
    }


    //Publish
    if(!publishKeypoints())
    {
        cout<<"error publishing"<<endl;
        return false;
    }
    

    return true;
}


void DroneKeypointsGridDetectorROSModule::droneImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    if(!moduleStarted)
        return;

    //Transform message to Opencv
    try
    {
        cvDroneImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    droneImage=cvDroneImage->image;


    //Run -> ashyncronous
    if(!run())
    {
        cout<<"Error running"<<endl;
        return;
    }


    return;
}



bool DroneKeypointsGridDetectorROSModule::publishKeypoints()
{
    if(droneModuleOpened==false)
        return false;


    //Transform to message
    droneMsgsROS::vectorPoints2DInt keypointsDetectedMsg;
    //Header: the header of the image
    keypointsDetectedMsg.header.stamp=cvDroneImage->header.stamp;
    //keypoints
    for (std::vector<cv::Point>::iterator it = gridIntersections.begin() ; it != gridIntersections.end(); ++it)
    {
        droneMsgsROS::vector2i point2D;
        point2D.x=it->x;
        point2D.y=it->y;
        keypointsDetectedMsg.point2DInt.push_back(point2D);
    }


    //Publish
    droneKeypointsPubl.publish(keypointsDetectedMsg);


    return true;

}









////// DroneGroundRobotsDetectorROSModule ///////////////

DroneGroundRobotsDetectorROSModule::DroneGroundRobotsDetectorROSModule() : DroneModule(droneModule::active)
{

    return;
}


DroneGroundRobotsDetectorROSModule::~DroneGroundRobotsDetectorROSModule()
{
    close();
    return;
}

int DroneGroundRobotsDetectorROSModule::setCalibCamera(std::string camCalibParamFile)
{
    this->camCalibParamFile=camCalibParamFile;

    return 1;
}

int DroneGroundRobotsDetectorROSModule::setBotParameters(std::string botParamFile)
{
    this->botParamFile=botParamFile;

    return 1;
}

int DroneGroundRobotsDetectorROSModule::setTopicConfigs(std::string camSubsTopicName, std::string groundRobotsPublTopicName)
{
    this->camSubsTopicName=camSubsTopicName;
    this->groundRobotsPublTopicName=groundRobotsPublTopicName;

    return 1;
}

int DroneGroundRobotsDetectorROSModule::setCVConfigs(std::string violaJonesModel, std::string svmModel, std::string svmScaleValues)
{
    this->violaJonesModel=violaJonesModel;
    this->svmModel=svmModel;
    this->svmScaleValues=svmScaleValues;

    return 1;
}


bool DroneGroundRobotsDetectorROSModule::init()
{
    DroneModule::init();


    /////Init cv module

    // Camera parameters
    IA14B.initializeCameraParameters(camCalibParamFile);


    // CSVM
    cv::Size csvmWorkSize=cv::Size(140,105);	// reduce the size of the frame for speeding up segmentation
    IA14B.initializeColorSVM_segmentation(svmModel, svmScaleValues, csvmWorkSize, cv::SVM::LINEAR, CV_BGR2YUV);


    // Load parameters from XML file
    // BOT-RELATED
    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(botParamFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        std::cout<<"Error opening bot parameter file"<<std::endl;
        return false;
    }


    /////////////////////////////////////
    // BOT DETECTION/TRACKING
    cv::Size minBotSize, maxBotSize;
    cv::Size xRASW, yRASW;
    double scaleFactor;
    int minNeighbors;
    int successiveFramesToTrack;
    int maxBotsToTrack;

    std::string readingValue;
    pugi::xml_node BOTSIZES = doc.child("botSizes");
    pugi::xml_node RASW = doc.child("RASW");
    pugi::xml_node VJ = doc.child("ViolaJones");
    pugi::xml_node TRACKER = doc.child("CamShift");

    readingValue=BOTSIZES.child_value("minValue");
    std::istringstream convertedValue1(readingValue);
    convertedValue1>> minBotSize.width;
    convertedValue1>> minBotSize.height;
    readingValue=BOTSIZES.child_value("maxValue");
    std::istringstream convertedValue2(readingValue);
    convertedValue2>> maxBotSize.width;
    convertedValue2>> maxBotSize.height;

    readingValue=RASW.child_value("mediumSpeedStageX");
    std::istringstream convertedValue3(readingValue);
    convertedValue3>> xRASW.width;
    readingValue=RASW.child_value("mediumSpeedStageY");
    std::istringstream convertedValue4(readingValue);
    convertedValue4>> yRASW.width;
    readingValue=RASW.child_value("slowSpeedStageX");
    std::istringstream convertedValue5(readingValue);
    convertedValue5>> xRASW.height;
    readingValue=RASW.child_value("slowSpeedStageY");
    std::istringstream convertedValue6(readingValue);
    convertedValue6>> yRASW.height;

    readingValue=VJ.child_value("scaleFactor");
    std::istringstream convertedValue7(readingValue);
    convertedValue7>> scaleFactor;
    readingValue=VJ.child_value("minNeighbors");
    std::istringstream convertedValue8(readingValue);
    convertedValue8>> minNeighbors;

    readingValue=TRACKER.child_value("maxBotsToTrack");
    std::istringstream convertedValue9(readingValue);
    convertedValue9>> maxBotsToTrack;
    readingValue=TRACKER.child_value("successiveFramesToTrack");
    std::istringstream convertedValue10(readingValue);
    convertedValue10>> successiveFramesToTrack;


    IA14B.initializeDetectionModule(violaJonesModel, minBotSize, maxBotSize, xRASW, yRASW, scaleFactor, minNeighbors, 1,
        successiveFramesToTrack, maxBotsToTrack);


    /////////////////////////////////////
    // BOT IDENTIFICATION
    double HoughMin_voteInputHeight_ratio, HoughVerticalROI_extensionFactor;
    int HoughMinAngle_degrees, HoughMaxAngle_degrees;
    cv::Scalar whiteThreshold=cv::Scalar(0, 0, 0), redThreshold=cv::Scalar(0, 0, 0), greenThreshold=cv::Scalar(0, 0, 0);
    bool useColorClues, useHoughClues;

    pugi::xml_node POLE = doc.child("poleParameters");
    pugi::xml_node COLOR = doc.child("colorParameters");
    pugi::xml_node ACTIVE = doc.child("activation");

    readingValue=POLE.child_value("ROI_verticalExtensionFactor");
    std::istringstream convertedValue01(readingValue);
    convertedValue01>> HoughVerticalROI_extensionFactor;
    readingValue=POLE.child_value("relativePoleHeight");
    std::istringstream convertedValue02(readingValue);
    convertedValue02>> HoughMin_voteInputHeight_ratio;
    readingValue=POLE.child_value("minPoleAngle");
    std::istringstream convertedValue03(readingValue);
    convertedValue03>> HoughMinAngle_degrees;
    readingValue=POLE.child_value("maxPoleAngle");
    std::istringstream convertedValue04(readingValue);
    convertedValue04>> HoughMaxAngle_degrees;

    readingValue=COLOR.child_value("whiteColorR");
    std::istringstream convertedValue05(readingValue);
    convertedValue05>> whiteThreshold[0];
    readingValue=COLOR.child_value("whiteColorG");
    std::istringstream convertedValue06(readingValue);
    convertedValue06>> whiteThreshold[1];
    readingValue=COLOR.child_value("whiteColorB");
    std::istringstream convertedValue07(readingValue);
    convertedValue07>> whiteThreshold[2];

    readingValue=COLOR.child_value("redColorR");
    std::istringstream convertedValue08(readingValue);
    convertedValue08>> redThreshold[0];
    readingValue=COLOR.child_value("redColorG");
    std::istringstream convertedValue09(readingValue);
    convertedValue09>> redThreshold[1];
    readingValue=COLOR.child_value("redColorB");
    std::istringstream convertedValue010(readingValue);
    convertedValue010>> redThreshold[2];

    readingValue=COLOR.child_value("greenColorR");
    std::istringstream convertedValue011(readingValue);
    convertedValue011>> greenThreshold[0];
    readingValue=COLOR.child_value("greenColorG");
    std::istringstream convertedValue012(readingValue);
    convertedValue012>> greenThreshold[1];
    readingValue=COLOR.child_value("greenColorB");
    std::istringstream convertedValue013(readingValue);
    convertedValue013>> greenThreshold[2];

    readingValue=ACTIVE.child_value("colorClues");
    std::istringstream convertedValue014(readingValue);
    convertedValue014>> useColorClues;

    readingValue=ACTIVE.child_value("poleClues");
    std::istringstream convertedValue015(readingValue);
    convertedValue015>> useHoughClues;


    IA14B.initializeIdentificationModule(whiteThreshold, greenThreshold, redThreshold, HoughMin_voteInputHeight_ratio,
        HoughMinAngle_degrees, HoughMaxAngle_degrees, HoughVerticalROI_extensionFactor, useColorClues, useHoughClues);
		
	
	/////////////////////////////////
	// BOT LABELLING
	int maxAllowedDrift;
	bool useTypeMatching;
	int labellerMaxSkippedFrames;
	
	pugi::xml_node LABELLER = doc.child("Labeller");

	readingValue=LABELLER.child_value("maximumDrift");
    std::istringstream convertedValue001(readingValue);
    convertedValue001>> maxAllowedDrift;
    readingValue=LABELLER.child_value("useTypeMatching");
    std::istringstream convertedValue002(readingValue);
    convertedValue002>> useTypeMatching;
    readingValue=LABELLER.child_value("maxSkippedFrames");
    std::istringstream convertedValue003(readingValue);
    convertedValue003>> labellerMaxSkippedFrames;
	
	IA14B.initializeLabellingModule(maxAllowedDrift, useTypeMatching, labellerMaxSkippedFrames);



    //end
    return true;
}


void DroneGroundRobotsDetectorROSModule::open(ros::NodeHandle & nIn)
{
    //Node
    DroneModule::open(nIn);


    //Camera parameters
    std::string camera_calib_param;
    ros::param::get("~camera_calib_param", camera_calib_param);
    cout<<"camera_calib_param="<<camera_calib_param<<endl;
    if ( camera_calib_param.length() == 0)
    {
        cout<<"[ROSNODE] Error with camera calibration paramaters"<<endl;
    }

    //Bot parameters
    std::string bot_param;
    ros::param::get("~bot_param", bot_param);
    cout<<"bot_param="<<bot_param<<endl;
    if ( bot_param.length() == 0)
    {
        cout<<"[ROSNODE] Error with bot paramaters"<<endl;
    }

    //Camera subscription topic
    std::string camera_topic_name;
    ros::param::get("~camera_topic_name",camera_topic_name);
    cout<<"camera_topic_name="<<camera_topic_name<<endl;
    if (camera_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with video subscription topic";
    }

    //ground_robots_topic_name
    std::string ground_robots_topic_name;
    ros::param::get("~ground_robots_topic_name",ground_robots_topic_name);
    cout<<"ground_robots_topic_name="<<ground_robots_topic_name<<endl;
    if (ground_robots_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with publisher topic";
    }

    std::string viola_jones_model;
    ros::param::get("~viola_jones_model",viola_jones_model);
    cout<<"viola_jones_model="<<viola_jones_model<<endl;
    if (viola_jones_model.length()==0)
    {
        cout<<"[ROSNODE] Error with viola_jones_model";
    }

    std::string svm_model;
    ros::param::get("~svm_model",svm_model);
    cout<<"svm_model="<<svm_model<<endl;
    if (svm_model.length()==0)
    {
        cout<<"[ROSNODE] Error with svm_model";
    }

    std::string svm_scale_values;
    ros::param::get("~svm_scale_values",svm_scale_values);
    cout<<"svm_scale_values="<<svm_scale_values<<endl;
    if (svm_scale_values.length()==0)
    {
        cout<<"[ROSNODE] Error with svm_scale_values";
    }


    if(!setCalibCamera(camera_calib_param))
        return;

    if(!setTopicConfigs(camera_topic_name,ground_robots_topic_name))
        return;

    if(!setBotParameters(bot_param))
        return;

    if(!setCVConfigs(viola_jones_model,svm_model,svm_scale_values))
        return;



    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }



    //// TOPICS
    //Subscribers
    droneImageSubs = n.subscribe(camSubsTopicName, 1, &DroneGroundRobotsDetectorROSModule::droneImageCallback, this);



    //Publishers
    droneGroundRobotsPubl=n.advertise<droneMsgsROS::vectorTargetsInImageStamped>(groundRobotsPublTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;

    //End
    return;
}


void DroneGroundRobotsDetectorROSModule::close()
{
    DroneModule::close();



    //Do stuff

    return;
}


bool DroneGroundRobotsDetectorROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool DroneGroundRobotsDetectorROSModule::startVal()
{
    //Do stuff

    //End
    return DroneModule::startVal();
}


bool DroneGroundRobotsDetectorROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool DroneGroundRobotsDetectorROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;


    //Process image
    //Clean
    groundRobotsROIs.clear();
    groundRobotsType.clear();
    groundRobotsId.clear();

    cv::Mat undistortedFrame;

    int errorCodeB=IA14B.run(droneImage, undistortedFrame, groundRobotsROIs, groundRobotsType, groundRobotsId);

    std::cout << "Error Code is : " << errorCodeB << "\n";

#ifdef CV_DEBUG
    // show the results
    cv::Mat results=undistortedFrame.clone();
    IA14B.showResults(results, groundRobotsROIs, groundRobotsId);
    cv::imshow("bots", results);
    cv::waitKey(1);
#endif

    // check for completion of IA14B.run()
    if(errorCodeB<0)
    {
        //return false;
    }



    //Publish
    if(!publishGroundRobots())
    {
        cout<<"error publishing"<<endl;
        return false;
    }


    return true;
}


void DroneGroundRobotsDetectorROSModule::droneImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    if(!moduleStarted)
        return;

    //Transform message to Opencv
    try
    {
        cvDroneImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    droneImage=cvDroneImage->image;


    //Run -> ashyncronous
    if(!run())
    {
        cout<<"Error running"<<endl;
        return;
    }


    return;
}



bool DroneGroundRobotsDetectorROSModule::publishGroundRobots()
{
    if(droneModuleOpened==false)
        return false;


    //Transform to message
    droneMsgsROS::vectorTargetsInImageStamped groundRobotsDetectedMsg;
    //Header
    groundRobotsDetectedMsg.header.stamp=ros::Time::now();
    //keypoints
    std::vector<cv::Rect>::iterator it1; //roi
    std::vector<int>::iterator it2; //type
    std::vector<int>::iterator it3; //id
    for ( it1 = groundRobotsROIs.begin(), it2 = groundRobotsType.begin(), it3 = groundRobotsId.begin(); it1 != groundRobotsROIs.end() && it2 != groundRobotsType.end() && it3 != groundRobotsId.end(); ++it1, ++it2, ++it3)
    {

        droneMsgsROS::targetInImage targetInImage;

        //ROI
        targetInImage.x=it1->x;
        targetInImage.y=it1->y;
        targetInImage.height=it1->height;
        targetInImage.width=it1->width;

        //cout<<targetInImage.x<<";"<<targetInImage.y<<endl;

        //type
        targetInImage.type=*it2;

        //id
        targetInImage.id=*it3;

        //Push
        groundRobotsDetectedMsg.targetsInImage.push_back(targetInImage);
    }


    //Publish
    droneGroundRobotsPubl.publish(groundRobotsDetectedMsg);




    return true;

}



